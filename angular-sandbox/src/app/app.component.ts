import { Component, OnInit } from '@angular/core';

export interface CompetenceCategory {
  title: string;
  competenceList?: Competence[];
}

export interface Competence {
  title: string;
  criteria: string[];
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  fileRead: any;
  lines: any[];
  competenceCategories: CompetenceCategory[];

  constructor() { }

  ngOnInit(): void {

  }


  csvInputChange(fileInput: any) {
    // read file from input
    this.fileRead = fileInput.target.files[0];

    const reader: FileReader = new FileReader();
    reader.readAsText(this.fileRead);

    reader.onload = (_) => {
      const csv: any = reader.result;
      const allTextLines = csv.split(/\r|\n|\r/);
      const headers = allTextLines[0].split(';');
      this.lines = [];

      for (const line of allTextLines) {
        // split content based on comma
        const data = line.split(';');
        if (data.length === headers.length) {
          if (data[0] !== headers[0]) {
            this.lines.push(data);
          }
        }
      }

      this.competenceCategories = [];

      this.lines.forEach(Data => {
        if (this.competenceCategories.find(e => e.title === Data[2]) === undefined) {
          const obj = {
            title: Data[2],
            competenceList: []
          };
          const cmptArr = [];
          this.lines.forEach(DataCPT => {
            if (DataCPT[2] === Data[2]) {
              if (cmptArr.find(eDataCPT => eDataCPT.title === DataCPT[3]) === undefined) {
                const cptObj = {
                  title: DataCPT[3],
                  criteria: []
                };
                const tmpCtrArr = [];
                this.lines.forEach(DataCTR => {
                  if (DataCPT[2] === Data[2] && DataCTR[3] === DataCPT[3]) {
                    if (tmpCtrArr.find(eDataCTR => eDataCTR === DataCTR[4]) === undefined) {
                      tmpCtrArr.push(DataCTR[4]);
                    }
                  }
                });
                cptObj.criteria = tmpCtrArr;
                cmptArr.push(cptObj);
              }
            }
          });
          obj.competenceList = cmptArr;
          this.competenceCategories.push(obj);
          console.log(this.competenceCategories)
        }
      });
    };
  }
}
