import { Component, OnInit, Input, AfterViewInit, OnDestroy } from '@angular/core';
import { CompetenceCategory, Competence } from '../app.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-form-example',
  templateUrl: './form-example.component.html',
  styleUrls: ['./form-example.component.scss']
})
export class FormExampleComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() $competenceCategories: CompetenceCategory[];
  filterForm: FormGroup;
  competenceList: Competence[];

  levelList: Array<any> = [];

  competenceCategorySubscription: Subscription;
  competencesSubscription: Subscription;

  exampleResult = {
    competenceCategory: 'C1',
    competencesWithLevel: [{
      competence: 'CPT1',
      levelNo: 3
    }, {
      competence: 'CPT2',
      levelNo: 1
    }]
  };

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.buildFilterForm();

  }

  ngAfterViewInit(): void {
    this.changingFilterFormCheck();
  }

  ngOnDestroy(): void {
    this.competenceCategorySubscription.unsubscribe();
    this.competencesSubscription.unsubscribe();
  }

  private buildFilterForm() {
    this.filterForm = this.fb.group({
      competenceCategory: [],
      competencesWithLevel: [],
    });
  }

  get competenceCategory() {

    return this.filterForm.get('competenceCategory');
  }

  get competencesWithLevel() {
    return this.filterForm.get('competencesWithLevel');
  }

  private changingFilterFormCheck() {
    this.competenceCategorySubscription = this.competenceCategory.valueChanges.subscribe(_ => {
      if (this.competenceCategory.dirty) {
        this.competenceCategory.markAsPristine();
        this.competenceList = this.filterCompetences(this.competenceCategory.value);
      }

    });

    this.competencesSubscription = this.competencesWithLevel.valueChanges.subscribe(_ => {
      if (this.competencesWithLevel.dirty) {
        this.competencesWithLevel.markAsPristine();
      }
    });
  }

  filterCompetences(cC: string): Competence[] {
    return this.$competenceCategories.find(d => d.title === cC).competenceList;
  }

  onSelect() {
    console.log(this.filterForm.value);
  }

  logChange(event: any, competenceTitle: string) {
    const competenceLevel = {
      compentence: competenceTitle,
      level: event.value
    };
    this.levelList.push(competenceLevel);
    console.log('level list: ', this.levelList);
  }

}
